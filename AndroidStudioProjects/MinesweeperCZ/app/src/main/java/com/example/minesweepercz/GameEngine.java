package com.example.minesweepercz;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.minesweepercz.util.Generator;
import com.example.minesweepercz.util.PrintGrid;
import com.example.minesweepercz.views.grid.Cell;

public class GameEngine {
    private static GameEngine instance;

    public static final int BOMB_NUMBER = 20; //určuje počet min
    public static final int WIDTH = 10; //určuje šířku tabulky
    public static final int HEIGHT = 15; //určuje výšku tabulky

    private Context context;

    private Cell[][] MinesweeperGrid = new Cell[WIDTH][HEIGHT];

    public static GameEngine getInstance() {
        if (instance == null) {
            instance = new GameEngine();
        }
        return instance;
    }

    private GameEngine() {    }

    public void createGrid(Context context) {
        Log.e("GameEngine","createGrid is working"); //trobbleshooting - vypíše se do
        this.context = context;                                 // "run" kódu, když třída funguje

        //vytvoření mřížky
        int[][] GeneratedGrid = Generator.generate(BOMB_NUMBER, WIDTH, HEIGHT); //vytváří logickou
                                                                                // mřížku
        PrintGrid.print(GeneratedGrid,WIDTH,HEIGHT); //vypisuje mřížku v souboru
        setGrid(context,GeneratedGrid);              //PrintGrid pomocí třídy print
    }

    private void setGrid(final Context context, final int[][] grid) {
        for(int x = 0; x < WIDTH; x ++ ){
            for(int y = 0; y < HEIGHT; y++) {
                if(MinesweeperGrid[x][y] == null) {
                    MinesweeperGrid[x][y] = new Cell( context, x, y);
                }
                MinesweeperGrid[x][y].setValue(grid[x][y]);
                MinesweeperGrid[x][y].invalidate();
            }
        }
    }

    public Cell getCellAt(int position) {
        int x = position % WIDTH;
        int y = position / WIDTH;
        return MinesweeperGrid[x][y]; //vrací mřížku
    }

    public Cell getCellAt(int x, int y) {
        return MinesweeperGrid[x][y];
    }

    public void click( int x, int y ) {
        if ( x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT && !getCellAt(x,y).isClicked() ) {
            getCellAt(x,y).setClicked();

            if(getCellAt(x,y).getValue() == 0) { //když klikneme na tlačítko bez bomby a bez čísla
                for(int xt = -1; xt <= 1; xt++) {
                    for(int yt = -1; yt <= 1; yt++) {
                        if(xt != yt) {
                            click(x + xt, y + yt);
                        }
                    }
                }
            }

            if(getCellAt(x,y).isBomb() ) { //zjišťuje, jestli bylo kliknuto na bombu
                onGameLost();           //vyvolává funkci na konec hry
            }
        }
        checkEnd();
    }

    private boolean checkEnd() {
        int bombNotFound = BOMB_NUMBER;
        int notRevealed = WIDTH * HEIGHT;
        for( int x = 0; x < WIDTH; x++) {
            for(int y = 0; y < HEIGHT; y++) {
                if( getCellAt(x,y).isRevealed() ||  getCellAt(x,y).isFlagged() ) {
                    notRevealed--;
                }
                if(getCellAt(x,y).isFlagged() && getCellAt(x,y).isBomb()) {
                    bombNotFound--;
                }
            }
        }
        if(bombNotFound == 0 && notRevealed == 0) {
            Toast.makeText(context,"You just won the game!", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public void flag (int x, int y) {                   //funkce kreslí a odkresluje vlajky..
        boolean isFlagged = getCellAt(x,y).isFlagged();
        getCellAt(x,y).setFlagged(!isFlagged);
        getCellAt(x,y).invalidate();
    }

    private void onGameLost() {
        Toast.makeText(context,"Game Over!", Toast.LENGTH_SHORT).show();
        for( int x = 0; x < WIDTH; x++) {
            for( int y = 0; y < HEIGHT; y++) {
                getCellAt(x,y).setRevealed();
            }
        }
    }
}