package com.example.minesweepercz.views.grid;

import android.content.Context;
import android.view.View;
import com.example.minesweepercz.GameEngine;

public class BaseCell extends View {

    private int value; //čísla -1 -> 9 (mínus jedničky pro bomby)
    private boolean isBomb; //nachází se zde bomba? = true, ne? = flase
    private boolean isRevealed; //když klikneme na nulu, odkryjí se všechny nuly a čísla 1-9 okolo
    private boolean isClicked; //co se stane když kliknu na pole
    private boolean isFlagged; //označí/odoznačí vlajku

    private int x,y; //souřadnice
    private int position; //pozice pole

    public BaseCell(Context context) {
        super(context);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        isBomb = false; //nastavuje hodnotu všech 4 níže uvedených proměnných na false..
        isRevealed = false;
        isClicked = false;
        isFlagged = false;
        if (value == -1) { //určuje bomby tím, že prochází mřížku a hledá všechny pole s -1
            isBomb = true;
        }

        this.value = value;
    }

    public boolean isBomb() {
        return isBomb;
    }

    public boolean isRevealed() {
        return isRevealed;
    }

    public void setRevealed() {
        isRevealed = true;
        invalidate();
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked() {
        this.isClicked = true;
        this.isRevealed = true;
        invalidate();
    }

    public boolean isFlagged() {
        return isFlagged;
    }

    public void setFlagged(boolean flagged) { //označí pole vlajkou
        isFlagged = flagged;
    }

    public int getXpos() {
        return x;
    }

    public int getYpos() {
        return y;
    }

    public void setPosition(int x, int y) { //nastaví pozici pole na zadané souřadnice[x][y]
        this.x = x;
        this.y = y;
        this.position = y * GameEngine.WIDTH + x;
        invalidate();
    }
}
