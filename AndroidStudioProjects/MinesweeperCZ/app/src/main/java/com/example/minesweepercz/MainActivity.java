package com.example.minesweepercz;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) { //vytváří tabulku
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((TextView)findViewById (R.id.textView)).setText("Welcome to Minesweeper");

        Log.e("MainActivity","onCreate");
        GameEngine.getInstance().createGrid(this);
    }
}