package com.example.minesweepercz.util;

import java.util.Random;

public class Generator {

    public static int[][] generate( int bombnumber, final int width, final int height) {
        Random r = new Random(); //náhodné generování čísel

        int[][] grid = new int[width][height]; //vytváří novou tabulku
        for (int x = 0; x < width; x++) {
            grid[x] = new int[height]; //přidává řádky (výšku)
        }

        while (bombnumber > 0) {   // bombnumber je počet bomb, kolik chceme v celé mřížce mít (20)
            int x = r.nextInt(width); //vezme náhodnou Xovou souřadnici
            int y = r.nextInt(height); //vezme náhodnou Yovou souřadnici
            //-1 je bomba
            if (grid[x][y] != -1) { //pokud políčko se souřadnicemi [x][y] není bomba, tak -
                grid[x][y] = -1;    // - stane se bombou..
                bombnumber--; //odečte 1 z čísla, kolik bomb chceme (čili nyní (napoprvé) 20-1)
            }
        }
        grid = calculateNeighbours(grid,width,height); //volá funkci calculateNeighbours
        return grid;
    }

    private static int[][] calculateNeighbours(int[][] grid, final int width, final int height) {
        for( int x = 0; x < width; x++) {
            for( int y = 0; y < height; y ++) { //vypočítá počet sousednních políček
                grid[x][y] = getNeighbourNumber(grid,x,y,width,height); //vyvoláfunkci na zjištění
            }   //kolik bomb je v okolí->postupně projde každé políčko a zjistí, kolik bomb jekolem
        }
        return grid;
    }

    private static int getNeighbourNumber(final int grid[][], final int x,
    final int y, final int width, final int height) {
        if(grid[x][y] == -1) { //pokud je políčko bomba
            return -1;         //vrátí -1
        }
        int count = 0; //proměnná pro počet bomb v okolí
        //pokud je mina na:
        if(isMineAt(grid,x - 1,y + 1,width,height)) count++; //levé horní políčko
        if(isMineAt(grid,x       ,y + 1,width,height)) count++; //horní políčko
        if(isMineAt(grid,x + 1,y + 1,width,height)) count++; //pravé horní políčko
        if(isMineAt(grid,x - 1,y       ,width,height)) count++; //levé políčko
        if(isMineAt(grid,x + 1,y       ,width,height)) count++; //pravé políčko
        if(isMineAt(grid,x - 1,y - 1,width,height)) count++; //levé spodní políčko
        if(isMineAt(grid,x       ,y - 1,width,height)) count++; //spodní políčko
        if(isMineAt(grid,x + 1,y - 1,width,height)) count++; //pravé spodní políčko
        //přičte se 1 do count
        return count; //vrátí počet sousedních bomb
    }
    private static boolean isMineAt(final int[][] grid, final int x, final int y, final int width,
    final int height) {
        if(x >= 0 && y >= 0 && x < width && y < height) {
            if(grid[x][y] == -1) {
                return true; //jednodušše.. když je na políčku bomba, vrátí true, jinak false..
            }
        }
        return false;
    }
}
