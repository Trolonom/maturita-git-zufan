package com.example.minesweepercz.views.grid;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.example.minesweepercz.GameEngine;
import com.example.minesweepercz.R;

public class Cell extends BaseCell implements View.OnClickListener, View.OnLongClickListener {

    public Cell(Context context, int x, int y){
        super(context);

        setPosition(x,y);

        setOnClickListener(this);
        setOnLongClickListener(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    public void onClick(View v) {
        GameEngine.getInstance().click(getXpos(), getYpos());//získává souřadnice zmáčknutého
    }                                                        // tlačítka

    @Override
    public boolean onLongClick(View v) { //získává souřadnice dlouze zmáčknutého tlačítka
        GameEngine.getInstance().flag( getXpos(), getYpos() );
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d("Minesweeper" , "Cell::onDraw");
        drawButton(canvas);

        if(isFlagged()) {     //jestli je na poli vlajka -
            drawFlag(canvas); // - vyvolá funkci, která ji namaluje
        }
        else if(isRevealed() && isBomb() && !isClicked() ) { //jestli je políčko s bombou odkryté,
            drawNormalBomb(canvas);                          //ale není zmáčknuté, vyvolá funkci,
        }                                                    //která namaluje nevybuchlou bombu
        else {
            if( isClicked() ) {           //když je políčko zmáčklé:
                if( getValue() == -1 ) { // a hodnota je -1 (neboli bomba), tak -
                    drawBombExploaded(canvas); // - vyvolá funkci, která namaluje vybuchlou bombu
                }
                else {
                    drawNumber(canvas); //jinak vyvolá funkci, která namaluje číslo kolik bomb je
                }                       // v okolí
            }
            else{
                drawButton(canvas); //jestli není tlačítko zmáčknuté, vyvolá funkci, která namaluje
            }                       //tlačítko..
        }
    }

    private void drawBombExploaded(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.bomb_exploded);
        drawable.setBounds(0, 0, getWidth(), getHeight());
        drawable.draw(canvas);
    }

    private void drawFlag(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.flag);
        drawable.setBounds(0,0,getWidth(),getHeight());
        drawable.draw(canvas);
    }

    private void drawButton(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.button);
        drawable.setBounds(0,0,getWidth(),getHeight());
        drawable.draw(canvas);
    }

    private void drawNormalBomb(Canvas canvas) {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.bomb_normal);
        drawable.setBounds(0,0,getWidth(),getHeight());
        drawable.draw(canvas);
    }

    private void drawNumber( Canvas canvas) {
        Drawable drawable = null;

        switch(getValue() ) { //získá value políčka, podle value pak namaluje číslo
            case 0:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_0);
                break;
            case 1:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_1);
                break;
            case 2:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_2);
                break;
            case 3:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_3);
                break;
            case 4:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_4);
                break;
            case 5:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_5);
                break;
            case 6:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_6);
                break;
            case 7:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_7);
                break;
            case 8:
                drawable = ContextCompat.getDrawable(getContext(), R.drawable.number_8);
                break;
        }
        drawable.setBounds(0,0,getWidth(),getHeight());
        drawable.draw(canvas);
    }
}